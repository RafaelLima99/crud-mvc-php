<?php

namespace Views;

class MainView
{
	private $fileName;
	private $header;
	private $footer;

	const titulo = "Atividade MVC";

	public function __construct($fileName, $header = '_header', $footer = '_footer'){
		$this->fileName = $fileName;
		$this->header = $header;
		$this->footer = $footer;
	}
	
	public function render($arr = []){
		include('pages/templates/'.$this->header.'.php');
		include('pages/'.$this->fileName.'.php');
		include('pages/templates/'.$this->footer.'.php');
	}
}