<div class="jumbotron">
	<h1>Novo contato</h1>
</div>

<section>
	<div class="container">
		<form action="">
			<div class="form-group">
				<input type="text" name="nome" class="form-control" required>
			</div>

			<div class="form-group">
				<input type="number" name="numero" class="form-control" required> 
			</div>

			<div class="text-right">
				<button class="btn btn-lg btn-dark">Salvar</button>
			</div>
		</form>
	</div>
</section>
