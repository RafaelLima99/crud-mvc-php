<?php

namespace Controllers;
class ListaController extends Controller
{
	public function __construct(){
		$this->view = new \Views\MainView('lista');
	}

	public function executar(){
		$this->view->render(array('titulo' => "Página de listagem"));
	}
}